# No More Amazon

This is goes over other stores, online and physical, that you can buy from instead of Amazon, organised by market and region.

## Motive

Amazon is too big. Amazon stomps out smaller businesses using its economic power and influence; so many people know Amazon has basically everything, "so why shop anywhere else?" the average person would think.

If you are a store owner that sells a category of product that Amazon also has on their main site, you do feel the lower sales. If you have worked for diapers.com, you know how it feels.

Despite their overwhelming wealth, they are choosing to not treat their groundfloor employees well. Their employees are put under tight deadlines to meet company quotas no matter how injured they get from their bodies being overworked. They have done and can create smear campaigns even against their own employees killed on the job because of no proper training by Amazon itself to properly operate specialised equipment such as forklifts.

Small companies that sign up to be delivery drivers for Amazon are placed under similar strict quotas. If a driver of one of those small companies injure someone not involved with Amazon at all, the small company takes the legal beatings, not Amazon, therefore skirting around responsibilities in order to make more money. The small guys lose while Amazon keeps making more despite the reason for the incident ultimately being their fault.

They publicly denied their delivery drivers having to pee in bottles despite NUMEROUS accounts from their drivers themselves, only to later confirm that it does happen.

They make sure to bust unions against the company. They hire employees explicitly meant to spy on groundfloor employees to make sure they do not and cannot form a union.

During the rise of the COVID-19 pandemic in 2020, they gave little to no help for protection against the microns-large microörganism, while demanding that the employees still meet those same tight quotas.

The COVID-19 pandemic also put delivery drivers on even tighter deadlines since they then have to carry many more packages than before. More people ordering online means more packages to deliver.

Their response for their poor working conditions on their employees was the claustrophobic AmaZen box. Nothing else.

Their Ring doorbells feed video data to local police without owners' consent. No matter how you personally feel about the police, Amazon decided what to do with *your* own device that *you* purchased with *your* own money. If you can understand people getting angry that Apple pushed a U2 song or album without their consent, you should be able to understand that as well.

Amazon does not make it explicit that their Internet-of-Things (IoT) devices such as the Ring doorbells and the Echo Dot involve others' physical properties when within their vicinity. As an owner of such devices, you are aware that you consent to your face and voice going through Amazon's servers. But, you aren't explicitly told that others who may not want *anything* to do with Amazon will get themselves involved with Amazon if they come anywhere near those IoT devices. If you live with others and you do get those devices, you are essentially consenting *for them* to be involved with Amazon, even if those others want to or not. *Same things applies to other similar devices such as the Google Home, but this is focused on Amazon.*

The founder, Jeff Bezos, is not paying taxes; while you, an average person making *much* less than him, stress out during tax season over how much you'll have to pay and, if you do, how much of a financial hit it could be. You yourself may not feel much, but your friends are probably masking their tax-anxiety.

Amazon's turn-over rate is 150%, meaning that they are running out of people to hire. There is no good reason for a company's turn-over rate to be so high. If people want to continue working at Amazon so much, that turn-over rate should be lower than 100%.

Yet, despite all this, **they are still making money, as if all of that is totally fine and acceptable by everyone**. Why do we keep buying from one singular entity when smaller stores are struggling to keep up with lowering customer bases? And when their groundfloor employees are continually being treated like total crap?

## Intended audiences

This is meant for the average person to read through, no matter their technical know-how. This includes older folks that are struggling to grasp current technology.

I also mean for this to be as apolitical as possible so as many people can understand the reason for this. If something sounds to you something political as a left thing or a right thing, consider who's making the topic "political"; they may be trying to distract you from where the problem lies. One sole entity of malice that, for example, only gives 50 millilitres of water each day to dehydrated people, shouldn't make you think as if the very thing of only giving those 50 millilitres is something political or philosophical. They (the people) are dehydrated. They obviously need much more than 50 millilitres each day, and you would as well. The people are not being ungrateful or anything for needing what the body needs to survive.

Whether you are left or right, you enjoy going to that favourite local store. You want to help them keep their business afloat. It would be sad to see them go under.

## Forewarn

Like any other online stores, they have to keep records of your transaction details somewhere, usually digitally. These details are the target of bad actors (commonly known as "hackers"), who prod at online stores' security to steal the details.

Not all online stores keep all transaction details safe in some standard way. While Amazon may know to keep those records safe, other stores may not even do the basics. 

If an online store has a physical location you can visit, you can choose to pay in cash so no credit card details can be recorded, thereby eliminating the risk of your details possibly being kept unsecurely. There will be a record of the transaction, but nothing such as a credit card number will be kept.

You can choose to use a proxy online to pay for the physical good, but then you would have to trust that the proxy keep your details secure.

If cash is not possible, you can use [privacy.com](https://privacy.com) to use a dummy card number to pay without having to risk your real card's details being leaked. Keep in mind that your real card's details have to be kept as well, as well as your last 4 digits of your Social Security Number.

Loadable cards you can find in stores are also another way to pay for goods online. However, they will typically charge a loading fee to put money on the card; you end up spending a bit more than you would like.

*Re*loadable cards will require sensitive personal information to use them. That information has to be stored somewhere as well. The less info digitally recorded, the safer you are from those bad actors having said sensitive information.


\*\*\*
This is a work-in-progress. I got most of what I wanted to write so I can start well the next time I work on this. Check on the TODO.md file for what I've thought to list on it to improve this document.
\*\*\*